package adp2;
/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 */
import java.util.ArrayList;

public abstract class PrimeSieve {
	private int counter;

	public int getCounter() {
		return counter;
	}

	public void increment(int n) {
		counter = counter + n;
	}

	public void reset() {
		counter = 0;
	}

	public abstract ArrayList<In> sieve(int n);
}
