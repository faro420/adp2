package adp2;
/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 */
public interface PrimeNumber {
    boolean isPrime(long n);
}
