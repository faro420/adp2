package adp2;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 * In wrapper class for Integer
 */
public class In {
    private int n;

    public In(int num) {
        n = num;
    }

    public int getN() {
        return n;
    }

    @Override
    public boolean equals(Object other) {
        return n == ((In) other).getN();
    }
    
    @Override
    public String toString(){
        return ""+n;
    }
}
