package verfahren;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 * A class to determine whether a given number is prime
 */
import adp2.PrimeNumber;

public class IsPrime implements PrimeNumber {
	private int counter;

	public int getCounter() {
		return counter;
	}

	public void increment(int n) {
		counter = counter + n;
	}

	public void reset() {
		counter = 0;
	}

	public boolean isPrime(long n) {
		if (n == 2 || n == 3) {
			counter++;
			return true;
		}
		if (n % 2 == 0) {
			counter++;
			return false;
		}
		int size = (int) Math.sqrt(n);

		for (long i = 3; i <= size; i += 2) {
			counter++;
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
