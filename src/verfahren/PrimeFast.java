package verfahren;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 */
import java.util.ArrayList;

import adp2.In;
import adp2.PrimeSieve;

public class PrimeFast extends PrimeSieve {

	public ArrayList<In> sieve(int n) {
		int counter = 0;
		boolean[] array = new boolean[n + 1];
		for (int k = 0; k <= n; k++) {
			array[k] = true;
			counter++;
		}
		int size = (int) Math.sqrt(n);
		ArrayList<In> prime = new ArrayList<In>(size);
		int i, j;
		for (i = 2; i <= n; i++) {
			counter++;
			for (j = 2; j <= i; j++) {
				counter++;
				if ((i % j == 0) && (j != i)) {
					array[i] = false;
				}
			}
		}
		for (i = 2; i <= n; i++) {
			if (array[i]) {
				prime.add(new In(i));
			}
		}
		increment(counter);
		return prime;
	}
}
