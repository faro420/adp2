package verfahren;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 * Implementation of Sieve of Erastothenes
 */
import java.util.ArrayList;

import adp2.In;
import adp2.PrimeSieve;

public class PrimeRocket extends PrimeSieve {

	public ArrayList<In> sieve(int n) {
		int counter = 0;
		boolean[] array = new boolean[n + 1];
		for (int k = 0; k <= n; k++) {
			array[k] = true;
			counter++;
		}
		int i, j;

		int size = (int) Math.sqrt(n);
		ArrayList<In> prime = new ArrayList<In>(size);

		for (i = 2; i <= size; i++) {
			counter++;
			if (array[i] == true) {
				for (j = 2; i * j <= n; j++) {
					array[i * j] = false;
					counter++;
				}
			}
		}
		for (i = 2; i <= n; i++) {
			if (array[i]) {
				prime.add(new In(i));
			}
		}
		increment(counter);
		return prime;
	}
}
