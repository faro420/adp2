package tests;

import org.junit.Test;

import verfahren.PrimeFast;
import verfahren.PrimeRocket;
import verfahren.PrimeSlow;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de Test class for different
 *         implementations of PrimeSieve
 */
public class TestVerfahren {
	@Test
	public void testPrimeSlow() {
    PrimeSlow p = new PrimeSlow();
    System.out.println("PrimeSlow");
    System.out.println("expected result:[2, 3, 5, 7, 11, 13, 17, 19]");
    System.out.println("result:         "+p.sieve(20));
    System.out.println("==============================");
	}

	@Test
	public void testPrimeFast() {
	    PrimeFast p = new PrimeFast();
	    System.out.println("PrimeFast");
	    System.out.println("expected result:[2, 3, 5, 7, 11, 13, 17, 19]");
	    System.out.println("result:         "+p.sieve(20));
	    System.out.println("==============================");
	}

	@Test
	public void testPrimeRocket() {
	    PrimeRocket p = new PrimeRocket();
	    System.out.println("PrimeRocket");
	    System.out.println("expected result:[2, 3, 5, 7, 11, 13, 17, 19]");
	    System.out.println("result:         "+p.sieve(20));
	    System.out.println("==============================");
	}
}
