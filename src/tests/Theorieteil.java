package tests;

import verfahren.PrimeFast;
import verfahren.PrimeRocket;
import verfahren.PrimeSlow;
import adp2.PrimeSieve;

public class Theorieteil {
	PrimeSieve slow, fast, rocket;

	void init() {
		slow = new PrimeSlow();
		fast = new PrimeFast();
		rocket = new PrimeRocket();
	}

	void reset() {
		slow.reset();
		fast.reset();
		rocket.reset();
	}
	
	void print(int n){
		init();
		System.out.println("Slow");
		System.out.println("Upper limit:"+n);
		slow.sieve(n);
		System.out.println("Number of runs:"+slow.getCounter());
		System.out.println("==============================");
		System.out.println("Fast");
		System.out.println("Upper limit:"+n);
		fast.sieve(n);
		System.out.println("Number of runs:"+fast.getCounter());
		System.out.println("==============================");
		System.out.println("Rocket");
		System.out.println("Upper limit:"+n);
		rocket.sieve(n);
		System.out.println("Number of runs:"+rocket.getCounter());
		System.out.println("==============================");
		reset();
	}
	public static void main(String[] args) {
		Theorieteil t = new Theorieteil();
		t.print(23);
	}
}
