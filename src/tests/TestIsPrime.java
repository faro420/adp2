package tests;

/**
 * @author: Mir Farshid Baha
 * @email: mirfarshid.baha@haw-hamburg.de
 * Test class IsPrime
 */
import static org.junit.Assert.*;

import org.junit.Test;
import verfahren.IsPrime;
import adp2.PrimeNumber;

public class TestIsPrime {

	@Test
	public void test1() {
		PrimeNumber p = new IsPrime();
		assertEquals(p.isPrime(11), true);
		print((IsPrime)p,11);
		assertEquals(p.isPrime(13), true);
		print((IsPrime)p,13);
		assertEquals(p.isPrime(24), false);
		print((IsPrime)p,24);
		assertEquals(p.isPrime(65), false);
		print((IsPrime)p,65);
		assertEquals(p.isPrime(101), true);
		print((IsPrime)p,101);
		assertEquals(p.isPrime(203), false);
		print((IsPrime)p,203);
		assertEquals(p.isPrime(52889), true);
		print((IsPrime)p,52889);
	}
	public void print(IsPrime p, long n){
		System.out.printf("prime candidate:%d \nnumber of runs:%d \n",n,p.getCounter());
		System.out.println("=================");
		p.reset();
	}
}
